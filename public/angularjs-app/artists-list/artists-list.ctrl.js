angular.module("app").controller("artistController", ArtistController);

function ArtistController(ArtistFactory) {
    const vm = this;

    const offset = 0;
    const count = 5;

    ArtistFactory.getAllArtists(offset, count).then(function (result) {
        vm.artists = result;
    });
}