angular.module("app", ['ngRoute'])
    .config(routeConfig);

function routeConfig($routeProvider) {
    $routeProvider.when("/", {
        templateUrl: "angularjs-app/artists-list/artists-list.html",
        controller: "artistController",
        controllerAs: "vm"
    }).when("/artist/:artistId", {
        templateUrl: "angularjs-app/artist-detail/artist-detail.html",
        controller: "artistDetailController",
        controllerAs: "vm"
    });
}