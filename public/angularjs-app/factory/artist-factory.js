angular.module("app").factory("ArtistFactory", ArtistFactory);

const _url = '/api/artists';

function ArtistFactory($http) {
    return {
        getAllArtists: getAll,
        getArtist: getOne
    }

    function getAll(offset, count) {
        return $http.get(_url + "?offset=" + offset + "&count=" + count).then(complete).catch(failure);
    }

    function getOne(artistId) {
        return $http.get(_url + "/" + artistId).then(complete).catch(failure);
    }

    function complete(response) {
        return response.data;
    }

    function failure(error) {
        return error;
    }
}