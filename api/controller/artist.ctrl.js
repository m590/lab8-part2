const mongoose = require('mongoose');
const Artist = mongoose.model("Artist");

module.exports.getAllArtists = function (req, res) {
    let count = 3;
    if (req.query && req.query.count) {
        count = parseInt(req.query.count);
    }
    let offset = 0;
    if (req.query && req.query.offset) {
        offset = parseInt(req.query.offset);
    }
    if (isNaN(count) || isNaN(offset)) {
        res.status(400).json({content: 'Query parameter count and offset should be numbers.'});
        return;
    }

    Artist.find().skip(offset).limit(count).exec(function (err, artists) {
        const response = {
            status: 200,
            content: artists
        };

        if (err) {
            console.log('Error get artists', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else {
            res.status(response.status).json(response.content);
        }
    });

}

module.exports.getOneArtist = function (req, res) {
    const artistId = req.params.artistId;
    Artist.findById(artistId).exec(function (err, artist) {
        const response = {
            status: 200,
            content: artist
        };

        if (err) {
            console.log('Error get an artist', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!artist) {
            response.status = 400;
            response.content = {content: 'Artist not found'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.addArtist = function (req, res) {
    const newArtist = {
        name: req.body.name,
        bornYear: req.body.bornYear,
        country: req.body.country,
        albums: [],
        hits: req.body.hits
    };

    Artist.create(newArtist, function (err, artist) {
        const response = {
            status: 201,
            content: artist
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.fullUpdateArtist = function (req, res) {
    const artistId = req.params.artistId;

    const query = {
        $set: {
            name: req.body.name,
            bornYear: req.body.bornYear,
            country: req.body.country,
            hits: req.body.hits
        }
    }

    Artist.updateOne({_id: mongoose.Types.ObjectId(artistId)}, query, {}, function (err, artist) {
        const response = {
            status: 200,
            content: artist
        };

        if (err) {
            console.log('Error get an artist', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!artist) {
            response.status = 400;
            response.content = {content: 'Artist not found'};
        }
        res.status(response.status).json(response.content);
    })

}

module.exports.partialUpdateArtist = function (req, res) {
    const artistId = req.params.artistId;

    const query = {
        $set: {}
    }

    if (req.body.name) {
        query.$set.name = req.body.name;
    }
    if (req.body.bornYear) {
        query.$set.bornYear = parseInt(req.body.bornYear);
    }
    if (req.body.country) {
        query.$set.country = req.body.country;
    }
    if (req.body.hits) {
        query.$set.hits = req.body.hits;
    }

    Artist.updateOne({_id: mongoose.Types.ObjectId(artistId)}, query, {}, function (err, artist) {
        const response = {
            status: 200,
            content: artist
        };

        if (err) {
            console.log('Error get an artist', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!artist) {
            response.status = 400;
            response.content = {content: 'Artist not found'};
        }
        res.status(response.status).json(response.content);
    })
}

module.exports.deleteArtist = function (req, res) {
    const artistId = req.params.artistId;
    Artist.deleteOne({_id: mongoose.Types.ObjectId(artistId)}, function (err, artist) {
        const response = {
            status: 204,
            content: artist
        };

        if (err) {
            console.log('Error get an artist', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!artist) {
            response.status = 400;
            response.content = {content: 'Artist not found'};
        }
        res.status(response.status).json(response.content);
    })
}
